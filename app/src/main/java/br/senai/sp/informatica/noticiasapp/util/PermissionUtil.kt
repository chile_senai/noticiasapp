package br.senai.sp.informatica.noticiasapp.util

import android.app.Activity
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log

/**
 * Created by SENAI on 31/05/2017.
 */
object PermissionUtil {
    fun checarPermissao(activity: Activity, requestCode: Int, vararg permissoes: String): Boolean {

        val negadas: ArrayList<String> = ArrayList<String>()
        for (permissao: String in permissoes) {
            Log.w("PASSOU", "PASSOU"+ContextCompat.checkSelfPermission(activity, permissao))
            if (ContextCompat.checkSelfPermission(activity, permissao) != PackageManager.PERMISSION_GRANTED) {
                negadas.add(permissao)
            }
        }
        if (negadas.isEmpty()) {
            return true;
        } else {
            negadas.toArray()
            val vetorNegadas = emptyArray<String>()
            ActivityCompat.requestPermissions(activity, negadas.toArray(vetorNegadas), requestCode)
            return false
        }
    }
}