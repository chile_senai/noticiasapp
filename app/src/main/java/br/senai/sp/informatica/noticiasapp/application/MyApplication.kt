package br.senai.sp.informatica.noticiasapp.application

import android.app.Application
import android.content.Context

/**
 * Created by SENAI on 21/07/2017.
 */
class MyApplication : Application() {


    companion object {
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
    }

}