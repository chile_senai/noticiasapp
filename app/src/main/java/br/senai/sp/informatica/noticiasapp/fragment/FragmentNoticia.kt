package br.senai.sp.informatica.noticiasapp.fragment

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.VideoView
import br.senai.sp.informatica.noticiasapp.R
import br.senai.sp.informatica.noticiasapp.adapter.NoticiaAdapter
import br.senai.sp.informatica.noticiasapp.modelo.Curtida
import br.senai.sp.informatica.noticiasapp.modelo.Noticia
import br.senai.sp.informatica.noticiasapp.task.HandlerTask
import br.senai.sp.informatica.noticiasapp.task.HandlerTaskAdapter
import br.senai.sp.informatica.noticiasapp.task.TaskRest
import br.senai.sp.informatica.noticiasapp.util.JsonParser
import br.senai.sp.informatica.noticiasapp.util.PrefsUtil
import br.senai.sp.informatica.noticiasapp.util.RestAddress
import kotlin.coroutines.experimental.EmptyCoroutineContext.plus

/**
 * Created by SENAI on 31/05/2017.
 */
class FragmentNoticia : Fragment() {
    lateinit var recycler: RecyclerView
    lateinit var noticias: ArrayList<Noticia>
    lateinit var progress: ProgressBar
    lateinit var noticia: Noticia
    val parser = JsonParser<Noticia>(Noticia::class.java)
    val parserLike = JsonParser<Curtida>(Curtida::class.java)
    lateinit var swipe: SwipeRefreshLayout

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // infla a View no Fragment
        val view = inflater!!.inflate(R.layout.fragment_noticia, container, false)

        // recycler
        recycler = view.findViewById(R.id.recycler) as RecyclerView
        recycler.layoutManager = LinearLayoutManager(context)

        // progress
        progress = view.findViewById(R.id.progress_bar) as ProgressBar

        // swipe
        swipe = view.findViewById(R.id.swipe) as SwipeRefreshLayout
        swipe.setOnRefreshListener(refreshListener)

        // busca a lista de notícia na web
        carregarLista()
        return view
    }

    fun carregarLista() {
        // busca a lista de notícia na web
        TaskRest(TaskRest.RequestMethod.GET, handlerTaskListar, PrefsUtil.getToken(context)).execute(RestAddress.NOTICIA)
    }

    val handlerTaskListar = object : HandlerTaskAdapter() {
        override fun onPreHandle() {
            progress.visibility = View.VISIBLE
        }

        override fun onSuccess(valueRead: String) {
            noticias = parser.toList(valueRead, Array<Noticia>::class.java)
            recycler.adapter = NoticiaAdapter(context, noticias = noticias, listener = listener)
            progress.visibility = View.GONE
            swipe.isRefreshing = false
        }

        override fun onError(erro: Exception, codigo: Int) {
            Snackbar.make(view!!, "" + codigo + " - " + erro.message, Snackbar.LENGTH_SHORT).show()
            progress.visibility = View.GONE
        }
    }

    val listener = object : NoticiaAdapter.ClickListenerNoticia {
        override fun clickAprovar(noticia: Noticia) {
            this@FragmentNoticia.noticia = noticia
            val url = String.format(RestAddress.APROVAR_NOTICIA, noticia.id)
            TaskRest(TaskRest.RequestMethod.PUT, handlerTaskAprovar, PrefsUtil.getToken(context)).execute(url)
        }

        override fun clickCurtir(noticia: Noticia) {
            this@FragmentNoticia.noticia = noticia
            val url: String
            val metodo: TaskRest.RequestMethod
            if (noticia.usuarioCurtiu) {
                //url = String.format(RestAddress.DESCURTIR_NOTICIA, noticia.id)
                metodo = TaskRest.RequestMethod.DELETE
            } else {
                //url = String.format(RestAddress.CURTIR_NOTICIA, noticia.id)
                metodo = TaskRest.RequestMethod.POST
            }
            url = String.format(RestAddress.CURTIR_NOTICIA, noticia.id)
            Log.w("URL", url)
            TaskRest(metodo, handlerTaskCurtir, PrefsUtil.getToken(context)).execute(url)
        }

        override fun clickExcluir(noticia: Noticia) {
            this@FragmentNoticia.noticia = noticia
            val url = String.format(RestAddress.EXCLUIR_NOTICIA, noticia.id)
            TaskRest(TaskRest.RequestMethod.DELETE, handlerTaskExcluir, PrefsUtil.getToken(context)).execute(url)
        }
    }

    val handlerTaskCurtir = object : HandlerTaskAdapter() {
        override fun onSuccess(valueRead: String) {
            if (noticia.usuarioCurtiu) {
                noticia.qtdCurtidas = noticia.qtdCurtidas?.minus(1)
                noticia.usuarioCurtiu = false
            } else {
                noticia.usuarioCurtiu = true
                val curtidaRetorno = parserLike.toObject(valueRead)
                noticia.qtdCurtidas = curtidaRetorno.noticia?.qtdCurtidas
            }
            recycler.adapter.notifyDataSetChanged()
        }

        override fun onError(erro: Exception, codigo: Int) {
            Snackbar.make(view!!, "" + codigo + " - " + erro.message, Snackbar.LENGTH_SHORT).show()
            progress.visibility = View.GONE
        }
    }

    val handlerTaskExcluir = object : HandlerTaskAdapter() {
        override fun onSuccess(valueRead: String) {
            var adapter: NoticiaAdapter = recycler.adapter as NoticiaAdapter
            val indexOf = noticias.indexOf(noticia)
            adapter.noticias.remove(noticia)
            adapter.notifyItemRangeRemoved(indexOf, 1)
        }

        override fun onError(erro: Exception, codigo: Int) {
            Snackbar.make(view!!, "" + codigo + " - " + erro.message, Snackbar.LENGTH_SHORT).show()
            progress.visibility = View.GONE
        }
    }

    val handlerTaskAprovar = object : HandlerTaskAdapter() {
        override fun onSuccess(valueRead: String) {
            noticia.aprovada = true
            recycler.adapter.notifyDataSetChanged()
        }

        override fun onError(erro: Exception, codigo: Int) {
            Snackbar.make(view!!, "" + codigo + " - " + erro.message, Snackbar.LENGTH_SHORT).show()
            progress.visibility = View.GONE
        }
    }

    val refreshListener = object : SwipeRefreshLayout.OnRefreshListener {
        override fun onRefresh() {
            carregarLista()
        }
    }

}