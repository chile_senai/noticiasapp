package br.senai.sp.informatica.noticiasapp.util

import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader

/**
 * Created by SENAI on 23/05/2017.
 */
object ReadStreamUtil {
    fun readStream(inputStream: InputStream): String {
        try {
            val reader = BufferedReader(InputStreamReader(inputStream));
            val builder = StringBuilder();
            reader.lineSequence().forEach {
                builder.append(it);
            }
            reader.close();
            return builder.toString();
        } catch (e: Exception) {
            throw e;
        }
    }
}