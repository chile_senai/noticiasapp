package br.senai.sp.informatica.noticiasapp.util

import com.google.gson.Gson
import com.google.gson.GsonBuilder

import java.util.Arrays

/**
 * Created by SENAI on 25/05/2017.
 */

class JsonParser<T>(internal val tipoClasse: Class<T>) {
    internal var gsonBuilder = GsonBuilder().setDateFormat("dd/MM/yyyy hh:mm:ss")
    internal var gson = gsonBuilder.create()

    fun toObject(json: String): T {
        return gson.fromJson(json, tipoClasse)
    }

    // pelo fato de T ser genérico, o Type não pode ser recuperado, sendo necessária a referência de vetor da classe
    fun toList(json: String, classe: Class<Array<T>>): ArrayList<T> {
        var lista = ArrayList<T>(Arrays.asList(*gson.fromJson(json, classe)))
        return lista
    }

    fun fromObject(objeto: T): String {
        return gson.toJson(objeto)
    }

}
