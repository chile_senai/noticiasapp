package br.senai.sp.informatica.noticiasapp.modelo

/**
 * Created by SENAI on 23/05/2017.
 */
class Usuario {
    var id : Long? = null;
    var nome : String? = null;
    var login : String? = null;
    var senha : String? = null;
    var tipoUsuario : TipoUsuario? = null;
}