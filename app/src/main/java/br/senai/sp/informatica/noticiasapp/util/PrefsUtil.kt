package br.senai.sp.informatica.noticiasapp.util

import android.content.Context
import android.preference.PreferenceManager

/**
 * Created by SENAI on 02/06/2017.
 */
object PrefsUtil {
    fun saveToken(context: Context, token: String?) {
        val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
        editor.putString("token", token)
        editor.commit()
    }

    fun getToken(context: Context): String {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        return prefs.getString("token", "")
    }

    fun saveValues(context: Context, key: String, value: String?) {
        val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
        editor.putString(key, value)
        editor.commit()
    }

    fun getValues(context: Context, key: String): String {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        return prefs.getString(key, "")
    }
}