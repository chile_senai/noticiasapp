package br.senai.sp.informatica.noticiasapp.activity

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View.*;
import br.senai.sp.informatica.noticiasapp.R
import br.senai.sp.informatica.noticiasapp.adapter.MeuPagerAdapter
import br.senai.sp.informatica.noticiasapp.fragment.FragmentNoticia
import br.senai.sp.informatica.noticiasapp.modelo.Usuario
import br.senai.sp.informatica.noticiasapp.util.PermissionUtil

class MainActivity : AppCompatActivity() {
    lateinit var viewPager: ViewPager;
    lateinit var tabLayout: TabLayout;

    companion object {
        var usuarioLogado: Usuario? = Usuario()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        viewPager = findViewById(R.id.view_pager) as ViewPager;
        // define que o viewpager armazenará 1 página além da atual
        viewPager.offscreenPageLimit = 1
        // define o Adapter no viewpager
        viewPager.adapter = MeuPagerAdapter(supportFragmentManager);

        viewPager.addOnPageChangeListener(listenerPager)


        tabLayout = findViewById(R.id.tab_layout) as TabLayout
        tabLayout.setupWithViewPager(viewPager)

    }

    val listenerPager = object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(p0: Int) {}

        override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {}

        override fun onPageSelected(p0: Int) {
            if (p0 == 0) {
                val adapter = viewPager.adapter as MeuPagerAdapter
                val fragNoticia = adapter.getItem(p0) as FragmentNoticia
                fragNoticia.carregarLista()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.action_sair) {
            finish()
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}
