package br.senai.sp.informatica.noticiasapp.task

import android.os.AsyncTask
import android.util.Log
import br.senai.sp.informatica.noticiasapp.util.ReadStreamUtil
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection


/**
 * Created by SENAI on 23/05/2017.
 */
class TaskRest(method: RequestMethod, handler: HandlerTask, token: String? = null) : AsyncTask<String, Void, String>() {

    // enumeração para indicar o tipo de método na requisição
    enum class RequestMethod {GET, POST, PUT, DELETE }

    // variável para controlar possíveis exceções que possam ocorrer
    private lateinit var exception: Exception
    // variável para indicar o tipo de método a ser chamado
    private val method: RequestMethod = method
    // variável para armazenar o handler de eventos
    private val handler: HandlerTask = handler
    // variável para armazenar o token caso exista
    private val token: String? = token
    // variável para armazenar o código de resposta
    private var responseCode: Int = 0

    override fun onPreExecute() {
        // executa o método preHandle do handler de eventos
        handler.onPreHandle()
    }

    override fun doInBackground(vararg params: String?): String? {
        // variável que receberá o conteúdo lido na resposta da requisição
        var retorno: String? = null
        try {
            // o endereço será passado como a primeira String dos parâmetros
            val endereco = params[0]

            // cria um objeto URL a partir do endereço
            val url = URL(endereco)
            // extrai da URL uma conexão HTTP
            val connection = url.openConnection() as HttpURLConnection
            // tempo máximo de espera para conexão
            connection.connectTimeout = 15000
            // tempo máximo de espera para leitura
            connection.readTimeout = 15000
            // caso exista token acrescenta na requisição
            if (token != null) {
                connection.setRequestProperty("Authorization", token)
            }
            // define o tipo de requisição de acordo com o método recebido via construtor
            connection.requestMethod = method.toString()
            // caso seja um POST ou PUT, deverá haver um segundo parâmetro que corresponde ao json que deve ser enviado no corpo da requisição
            if ((method == RequestMethod.POST || method == RequestMethod.PUT) && params.size > 1) {
                // extrai o json da posição 1 do parâmetro
                val json = params[1]
                // define o ContentType da requisição
                connection.setRequestProperty("Content-Type", "application/json;charset=utf-8")
                // habilita a escrita na requisição
                connection.doOutput = true
                // obtém um objeto OutputStream para gravar dados na requisição
                val outputStream = connection.outputStream
                // escreve o JSON no corpo da requisição
                outputStream.write(json?.toByteArray(Charsets.UTF_8))
                // libera o output e fecha o recurso
                outputStream.flush()
                outputStream.close()
            }
            // obtém o status da requisição
            responseCode = connection.responseCode
            when (method) {
                RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT -> {
                    if (responseCode == HttpURLConnection.HTTP_CREATED || responseCode == HttpURLConnection.HTTP_OK) {
                        // obtém um inputStream para ler o corpo da resposta
                        val inputStream = connection.inputStream
                        // coloca na variável retorno a String lida no corpo da resposta
                        retorno = ReadStreamUtil.readStream(inputStream)
                        // fecha o inputStream
                        inputStream.close()
                    } else {
                        // dispara uma exceção
                        throw Exception("Erro no médodo " + method.toString())
                    }
                }
                RequestMethod.DELETE -> {
                    // a String vazia será usada para indicar no postExecute que a requsição foi feita com sucesso
                    if (responseCode == HttpURLConnection.HTTP_NO_CONTENT) {
                        retorno = ""
                    } else {
                        throw Exception("Erro no médodo " + method.toString())
                    }
                }
            }
            connection.disconnect()
        } catch (e: IndexOutOfBoundsException) {
            // uma IndexOuOfBounds ocorrerá caso não seja passado o segundo parâmetro quando a requisição for POST ou PUT
            exception = Exception("Está faltando um parâmetro para a execução do método")
        } catch (e: Exception) {
            // coloca na variável exception o erro para ser tratado pelo método no handler invocado no post execute
            exception = e
        }
        return retorno
    }

    override fun onPostExecute(result: String?) {
        if (result != null) {
            // chama o método do handler para sucesso
            handler.onSuccess(result)
        } else {
            // result só será nulo caso exista um erro, aqui disparamos o método que trata o erro
            handler.onError(exception, responseCode)
        }
    }
}