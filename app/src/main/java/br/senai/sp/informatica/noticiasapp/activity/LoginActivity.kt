package br.senai.sp.informatica.noticiasapp.activity

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import br.senai.sp.informatica.noticiasapp.R
import br.senai.sp.informatica.noticiasapp.modelo.Noticia
import br.senai.sp.informatica.noticiasapp.modelo.SecurityToken
import br.senai.sp.informatica.noticiasapp.modelo.Usuario
import br.senai.sp.informatica.noticiasapp.task.HandlerTaskAdapter
import br.senai.sp.informatica.noticiasapp.task.TaskRest
import br.senai.sp.informatica.noticiasapp.util.JsonParser
import br.senai.sp.informatica.noticiasapp.util.PrefsUtil
import br.senai.sp.informatica.noticiasapp.util.RestAddress

/**
 * Created by SENAI on 28/06/2017.
 */
class LoginActivity : AppCompatActivity() {
    lateinit var editEmail: EditText
    lateinit var editSenha: EditText
    lateinit var btLogin: Button
    lateinit var btSalvar: Button
    lateinit var editEndereco: EditText
    lateinit var imageIcone: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        // editLogin
        editEmail = findViewById(R.id.edit_email) as EditText

        // editSenha
        editSenha = findViewById(R.id.edit_senha) as EditText
        // associa o clique na opção do teclado IME com o clique do botão entrar
        editSenha.setOnEditorActionListener {
            view, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                btLogin.performClick()
            }
            false
        }

        // btLogin
        btLogin = findViewById(R.id.bt_logar) as Button

        // btSalvar
        btSalvar = findViewById(R.id.bt_salvar) as Button

        // editEndereco
        editEndereco = findViewById(R.id.edit_endereco) as EditText

        // imageIcone
        imageIcone = findViewById(R.id.image_icone) as ImageView

        imageIcone.setOnLongClickListener(object : View.OnLongClickListener {
            override fun onLongClick(v: View?): Boolean {
                editEndereco.visibility = View.VISIBLE
                btSalvar.visibility = View.VISIBLE
                return true;
            }
        })
    }

    fun salvarClick(v: View) {
        val endereco = editEndereco.text.toString()
        PrefsUtil.saveValues(this, "endereco", endereco)
        editEndereco.visibility = View.GONE
        btSalvar.visibility = View.GONE
    }

    fun cadastreClick(v: View) {
        val intent = Intent(this, CadastroActivity::class.java)
        startActivity(intent)
    }

    fun logarClick(v: View) {
        if (editEmail.text.isEmpty()) {
            Toast.makeText(this, R.string.valida_email, Toast.LENGTH_SHORT).show()
            editEmail.requestFocus()
        } else if (editSenha.text.isEmpty()) {
            Toast.makeText(this, R.string.valida_senha, Toast.LENGTH_SHORT).show()
            editSenha.requestFocus()
        } else {
            val usuario = Usuario()
            usuario.login = editEmail.text.toString()
            usuario.senha = editSenha.text.toString()
            PrefsUtil.saveValues(baseContext, "login", usuario.login);
            PrefsUtil.saveValues(baseContext, "senha", usuario.senha);
            val parser = JsonParser<Usuario>(Usuario::class.java)
            TaskRest(TaskRest.RequestMethod.POST, handlerTaskLogar).execute(RestAddress.LOGIN, parser.fromObject(usuario))
        }
    }

    val handlerTaskLogar = object : HandlerTaskAdapter() {
        override fun onError(erro: Exception, codigo: Int) {
            Toast.makeText(baseContext, "" + codigo + " - " + erro.message, Toast.LENGTH_SHORT).show()
        }

        override fun onSuccess(valueRead: String) {
            val parser = JsonParser<SecurityToken>(SecurityToken::class.java)
            val securityToken = parser.toObject(valueRead)
            PrefsUtil.saveToken(baseContext, securityToken.token)
            val intent = Intent(baseContext, MainActivity::class.java)
            MainActivity.usuarioLogado = securityToken.usuario
            startActivity(intent)
            finish()
        }
    }
}