package br.senai.sp.informatica.noticiasapp.task

/**
 * Created by SENAI on 23/05/2017.
 */
interface HandlerTask {
    fun onPreHandle();
    fun onSuccess(valueRead: String);
    fun onError(erro: Exception, codigo : Int = 0);
}