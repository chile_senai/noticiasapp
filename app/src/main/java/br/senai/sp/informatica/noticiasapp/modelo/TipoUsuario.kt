package br.senai.sp.informatica.noticiasapp.modelo

/**
 * Created by SENAI on 23/05/2017.
 */
enum class TipoUsuario {
    USUARIO_COMUM, MODERADOR
}