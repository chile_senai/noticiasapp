package br.senai.sp.informatica.noticiasapp.activity

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.*
import br.senai.sp.informatica.noticiasapp.R
import br.senai.sp.informatica.noticiasapp.modelo.TipoUsuario
import br.senai.sp.informatica.noticiasapp.modelo.Usuario
import br.senai.sp.informatica.noticiasapp.task.HandlerTaskAdapter
import br.senai.sp.informatica.noticiasapp.task.TaskRest
import br.senai.sp.informatica.noticiasapp.util.JsonParser
import br.senai.sp.informatica.noticiasapp.util.PrefsUtil
import br.senai.sp.informatica.noticiasapp.util.RestAddress

/**
 * Created by SENAI on 03/08/2017.
 */
class CadastroActivity : AppCompatActivity() {
    lateinit var editEmail: EditText
    lateinit var editSenha: EditText
    lateinit var editNome: EditText
    lateinit var btSalvar: Button
    lateinit var ckModerador: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cadastrese)

        editNome = findViewById(R.id.edit_nome) as EditText

        editEmail = findViewById(R.id.edit_email) as EditText

        editSenha = findViewById(R.id.edit_senha) as EditText

        ckModerador = findViewById(R.id.ck_moderador) as CheckBox
    }

    fun salvarClick(v: View) {
        if (editNome.text.isEmpty()) {
            Toast.makeText(this, R.string.valida_nome, Toast.LENGTH_SHORT).show()
            editNome.requestFocus()
        } else if (editEmail.text.isEmpty()) {
            Toast.makeText(this, R.string.valida_email, Toast.LENGTH_SHORT).show()
            editEmail.requestFocus()
        } else if (editSenha.text.isEmpty()) {
            Toast.makeText(this, R.string.valida_senha, Toast.LENGTH_SHORT).show()
            editSenha.requestFocus()
        } else {
            val usuario = Usuario()
            usuario.nome = editNome.text.toString()
            usuario.login = editEmail.text.toString()
            usuario.senha = editSenha.text.toString()
            if (ckModerador.isChecked)
                usuario.tipoUsuario = TipoUsuario.MODERADOR
            else
                usuario.tipoUsuario = TipoUsuario.USUARIO_COMUM
            val parser = JsonParser<Usuario>(Usuario::class.java)
            TaskRest(TaskRest.RequestMethod.POST, handlerTask).execute(RestAddress.NOVO_USUARIO, parser.fromObject(usuario))
        }
    }

    val handlerTask = object : HandlerTaskAdapter() {
        override fun onSuccess(valueRead: String) {
            Toast.makeText(baseContext!!, R.string.usuario_inserido, Toast.LENGTH_SHORT).show()
            finish()
        }

        override fun onError(erro: Exception, codigo: Int) {
            Toast.makeText(baseContext!!, "" + codigo + " - " + erro.message, Toast.LENGTH_SHORT).show()
        }
    }
}