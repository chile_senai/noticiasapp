package br.senai.sp.informatica.noticiasapp.util

import br.senai.sp.informatica.noticiasapp.application.MyApplication

object RestAddress {
    // endereço base da API
    private val URL = "http://" + PrefsUtil.getValues(MyApplication.context, "endereco") + "/noticias"
    // endereços dos recursos da API
    val NOTICIA = URL + "/noticia"
    val CURTIR_NOTICIA = NOTICIA + "/%d/curtir"
    val EXCLUIR_NOTICIA = NOTICIA + "/%d"
    val LOGIN = URL + "/logar"
    val NOVO_USUARIO = URL + "/usuario"
    val APROVAR_NOTICIA = NOTICIA + "/%d/aprovar"
}