package br.senai.sp.informatica.noticiasapp.modelo

import java.util.*

/**
 * Created by SENAI on 28/06/2017.
 */
class SecurityToken {
    var id: Long? = null
    var token: String? = null
    var expiracao: Date? = null
    var usuario: Usuario? = null
}