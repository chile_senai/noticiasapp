package br.senai.sp.informatica.noticiasapp.task

/**
 * Created by SENAI on 23/05/2017.
 */
abstract class HandlerTaskAdapter : HandlerTask {
    override fun onError(erro: Exception, codigo: Int) {}

    override fun onPreHandle() {}

    override fun onSuccess(valueRead: String) {}
}