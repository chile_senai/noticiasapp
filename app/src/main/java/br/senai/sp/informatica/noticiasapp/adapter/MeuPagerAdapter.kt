package br.senai.sp.informatica.noticiasapp.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import br.senai.sp.informatica.noticiasapp.fragment.FragmentAddNoticia
import br.senai.sp.informatica.noticiasapp.fragment.FragmentNoticia

/**
 * Created by SENAI on 24/05/2017.
 */
class MeuPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
    var frag0: Fragment? = null
    var frag1: Fragment? = null

    override fun getPageTitle(position: Int): CharSequence {
        when (position) {
            0 -> {
                return "Fofocas"
            }
            1 -> {
                return "Nova Fofoca"
            }
        }
        return ""
    }

    override fun getItem(p0: Int): Fragment {
        when (p0) {
            0 -> {
                if (frag0 == null)
                    frag0 = FragmentNoticia()
                return frag0 as Fragment
            }
            1 -> {
                if (frag1 == null)
                    frag1 = FragmentAddNoticia()
                return frag1 as Fragment
            }

        }
        return Fragment();
    }

    override fun getCount(): Int {
        return 2
    }
}