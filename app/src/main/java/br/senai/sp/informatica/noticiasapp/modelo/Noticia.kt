package br.senai.sp.informatica.noticiasapp.modelo

import java.util.*


class Noticia {
    var id: Long? = null
    var titulo: String? = null
    var texto: String? = null
    var usuario: Usuario? = null
    var aprovada: Boolean = false
    var dataCriacao : Date? = null;
    var qtdCurtidas: Int? = null;
    var usuarioCurtiu : Boolean = false
}
