package br.senai.sp.informatica.noticiasapp.adapter

import android.content.Context
import android.graphics.Color
import android.net.MailTo
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import br.senai.sp.informatica.noticiasapp.R
import br.senai.sp.informatica.noticiasapp.activity.MainActivity
import br.senai.sp.informatica.noticiasapp.modelo.Noticia
import br.senai.sp.informatica.noticiasapp.modelo.TipoUsuario
import java.text.SimpleDateFormat

/**
 * Created by SENAI on 31/05/2017.
 */
class NoticiaAdapter(val context: Context, val noticias: ArrayList<Noticia>, val listener: ClickListenerNoticia? = null) : RecyclerView.Adapter<NoticiaAdapter.NoticiaViewHolder>() {

    override fun onBindViewHolder(holder: NoticiaViewHolder, position: Int) {
        var proprioUsuario = false
        val noticia = noticias.get(position)
        val formatador = SimpleDateFormat("dd/MM/yyyy hh:mm:ss")
        holder.imageAprovar.visibility = View.GONE
        holder.textData.text = formatador.format(noticia.dataCriacao)
        holder.textTitulo.text = noticia.titulo
        holder.textAutor.text = noticia.usuario?.nome
        holder.textConteudo.text = noticia.texto
        holder.textLikes.text = "" + noticia.qtdCurtidas
        holder.imageExcluir.setOnClickListener {
            listener?.clickExcluir(noticia)
        }
        if (noticia.usuario?.id == MainActivity.usuarioLogado?.id) {
            proprioUsuario = true
            holder.imageExcluir.visibility = View.VISIBLE
        } else {
            holder.imageExcluir.visibility = View.GONE
            holder.imageView.setOnClickListener {
                listener?.clickCurtir(noticia)
            }
        }
        if (!noticia.aprovada) {
            holder.imageView.visibility = View.GONE
            holder.textLikes.visibility = View.GONE
            holder.textData.setBackgroundColor(Color.RED)
            holder.textTitulo.setBackgroundColor(Color.RED)
            holder.textData.setTextColor(Color.WHITE)
            holder.textTitulo.setTextColor(Color.WHITE)
            if (MainActivity.usuarioLogado?.tipoUsuario == TipoUsuario.MODERADOR && !proprioUsuario) {
                holder.imageAprovar.visibility = View.VISIBLE
                holder.imageAprovar.setOnClickListener {
                    listener?.clickAprovar(noticia)
                }
            }
            holder.imageExcluir.visibility = View.VISIBLE
        } else {
            holder.imageView.visibility = View.VISIBLE
            holder.textLikes.visibility = View.VISIBLE
            holder.textData.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
            holder.textTitulo.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
            holder.textData.setTextColor(ContextCompat.getColor(context, R.color.branco))
            holder.textTitulo.setTextColor(ContextCompat.getColor(context, R.color.branco))
        }

        if (noticia.usuarioCurtiu) {
            holder.imageView.setImageResource(R.drawable.ic_liked)
        } else {
            holder.imageView.setImageResource(R.drawable.ic_like)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): NoticiaViewHolder {
        // infla a view do layout
        val view = LayoutInflater.from(context).inflate(R.layout.card_noticia, parent, false)
        val holder = NoticiaViewHolder(view)
        return holder
    }

    override fun getItemCount(): Int {
        return noticias.size
    }

    interface ClickListenerNoticia {
        fun clickAprovar(noticia: Noticia)
        fun clickCurtir(noticia: Noticia)
        fun clickExcluir(noticia: Noticia)
    }

    // subclasse de ViewHolder
    class NoticiaViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var textTitulo: TextView
        var textConteudo: TextView
        var textAutor: TextView
        var textLikes: TextView
        var imageView: ImageView
        var btAprovar: Button
        var textData: TextView
        var imageExcluir: ImageView
        var imageAprovar: ImageView

        init {
            // cria as variáveis para usar no ViewHolder
            textData = v.findViewById(R.id.text_data) as TextView
            textTitulo = v.findViewById(R.id.text_titulo) as TextView
            textConteudo = v.findViewById(R.id.text_conteudo) as TextView
            textAutor = v.findViewById(R.id.text_autor) as TextView
            textLikes = v.findViewById(R.id.text_qtd_likes) as TextView
            imageView = v.findViewById(R.id.imageview) as ImageView
            btAprovar = v.findViewById(R.id.bt_aprovar) as Button
            imageExcluir = v.findViewById(R.id.imageExcluir) as ImageView
            imageAprovar = v.findViewById(R.id.imageAprovar) as ImageView
        }

    }
}