package br.senai.sp.informatica.noticiasapp.fragment

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import br.senai.sp.informatica.noticiasapp.R
import br.senai.sp.informatica.noticiasapp.modelo.Noticia
import br.senai.sp.informatica.noticiasapp.task.HandlerTaskAdapter
import br.senai.sp.informatica.noticiasapp.task.TaskRest
import br.senai.sp.informatica.noticiasapp.util.JsonParser
import br.senai.sp.informatica.noticiasapp.util.PrefsUtil
import br.senai.sp.informatica.noticiasapp.util.RestAddress

/**
 * Created by SENAI on 24/05/2017.
 */
class FragmentAddNoticia : Fragment() {
    lateinit var editTexto: EditText
    lateinit var btSalvar: Button
    lateinit var editTitulo: EditText

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // infla a view no fragment
        val view = inflater!!.inflate(R.layout.fragment_add_noticia, container, false)

        // edit do titulo da notícia
        editTitulo = view.findViewById(R.id.edit_titulo) as EditText
        // edit do conteúdo da notícia
        editTexto = view.findViewById(R.id.edit_texto) as EditText
        // ajustes feitos para aparecer o botão GO no teclado do Android
        editTexto.setHorizontallyScrolling(false)
        editTexto.setLines(6)
        editTexto.setMaxLines(Integer.MAX_VALUE)
        // associa o clique na opção do teclado IME com o clique do botão salvar
        editTexto.setOnEditorActionListener {
            view, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                btSalvar.performClick()
                true
            }
            false
        }

        // btSalvar
        btSalvar = view.findViewById(R.id.bt_salvar) as Button
        btSalvar.setOnClickListener {
            if (editTitulo.text.isEmpty()) {
                Toast.makeText(context, R.string.valida_titulo, Toast.LENGTH_SHORT).show()
                editTitulo.requestFocus()
            } else if (editTexto.text.isEmpty()) {
                Toast.makeText(context, R.string.valida_texto, Toast.LENGTH_SHORT).show()
                editTitulo.requestFocus()
            } else {
                val noticia = Noticia()
                noticia.texto = editTexto.text.toString()
                noticia.titulo = editTitulo.text.toString()
                val parser = JsonParser<Noticia>(Noticia::class.java)
                TaskRest(TaskRest.RequestMethod.POST, handlerTask, PrefsUtil.getToken(context)).execute(RestAddress.NOTICIA, parser.fromObject(noticia))
            }
        }

        editTitulo.requestFocus()
        // retorna a view
        return view;
    }

    fun limparCampos() {
        editTitulo.setText("")
        editTexto.setText("")
        editTitulo.requestFocus()
    }

    val handlerTask = object : HandlerTaskAdapter() {
        override fun onError(erro: Exception, codigo: Int) {
            Snackbar.make(view!!, "" + codigo + " - " + erro.message, Snackbar.LENGTH_SHORT).show()
        }

        override fun onSuccess(valueRead: String) {
            limparCampos()
            Snackbar.make(view!!, R.string.noticia_inserida, Snackbar.LENGTH_SHORT).show()
        }
    }

}